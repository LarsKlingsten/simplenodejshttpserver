"use strict"; // used with Ecmascript 6 //
var http = require('http');
var i = 0; 
var myContent = "Hello from Node.js";
console.log('server.js started 8000');
  
var myServer = http.createServer(function (request, response) {
   i++;
 //  console.log("Node.js req="+ i + ' url');
   response.writeHead(200,  {
      'Content-Length': myContent.length,
      'Content-Type': 'text/plain',
   });
   response.end(myContent); // + i);
   });
myServer.listen(8000);