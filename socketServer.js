"use strict"; // used with Ecmascript 6

// Load the net module to create a tcp server.
var net = require('net');

var counter = 0;
// Creates a new TCP server. The handler argument is automatically set as a listener for the 'connection' event
var server = net.createServer(function (socket) {

  counter++;
  // Every time someone connects, tell them hello and then close the connection.
  console.log("Connection from " + socket.remoteAddress + " " + socket.remotePort );
  socket.end("Hello World req="+ counter +" \n" + socket );
  
  

});

// Fire up the server bound to port 7000 on localhost
server.listen(7000, "localhost");

// Put a friendly message on the terminal
console.log("TCP server listening on port 7000 at localhost.");